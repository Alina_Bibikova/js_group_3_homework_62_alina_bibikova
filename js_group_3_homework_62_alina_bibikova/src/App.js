import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import GeneralContent from "./containers/GeneralContent/GeneralContent";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import Home from "./containers/Home/Home";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={GeneralContent}/>
                <Route path='/about' component={About}/>
                <Route path='/contacts' component={Contacts}/>
                <Route path='/home' component={Home}/>
            </Switch>
        </BrowserRouter>
    )
  }
}

export default App;
