import React, {Component, Fragment} from 'react';
import Header from "../../components/Header/Header";
import GoogleMapReact from 'google-map-react';
import './Contacts.css';


const AnyReactComponent = ({ text }) => <div>{text}</div>;

class Contacts extends Component {
    static defaultProps = {
        center: {
            lat: 42.875777,
            lng: 74.604764
        },
        zoom: 17
    };

    render() {
        return (
            <Fragment>
                <Header/>
                <div className='contacts'>
                    <h3>Contacts</h3>
                    <ul>
                        <li>720001, Kyrgyz Republic, Bishkek City</li>
                        <li>114 Chui avenue, room 427</li>
                        <li>Phone:  +996 312 90 19 57</li>
                        <li>Fax: +996 90 19 57</li>
                        <li>Cell phone: +996 555 51 59 75</li>
                        <li>E-mail: eastwindtravel@gmail.com</li>
                    </ul>
                </div>
                <div style={{ height: '100vh', width: '100%' }}>
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyBK0bvuueWQeMEK1FiTqR4hveCKp0AlRRY'}}
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}

                    >
                        <AnyReactComponent
                            lat={42.875777}
                            lng={74.604764}
                            text={'East Wind Travel'}
                        />
                </GoogleMapReact>
                </div>
            </Fragment>
        );
    }
}

export default Contacts;