import React, {Component, Fragment} from 'react';
import Header from "../../components/Header/Header";
import Carousel from "../Carousel/Carousel";

class Home extends Component {

    render () {
        return (
            <Fragment>
                <Header/>
                    <Carousel/>
            </Fragment>
        )
    }

}

export default Home;