import React, {Component, Fragment} from 'react';
import './GeneralContent.css';
import Header from "../../components/Header/Header";

import image from '../../components/Images/img.jpg';

class GeneralContent extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <div className='generalImg'>
                    <img className='generalPic' src={image} alt="img"/>
                </div>
            </Fragment>
        );
    }
}

export default GeneralContent;