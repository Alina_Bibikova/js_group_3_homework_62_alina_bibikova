import React, {Component, Fragment} from 'react';
import Header from "../../components/Header/Header";
import './About.css';


class About extends Component {

    render() {
        return (
            <Fragment>
                <Header/>
               <div className='about'>
                   <div className='items'>
                       <div className='item'>
                         <p>East Wind travel agency is a fast-developing company on tourism market of Kyrgyz Republic. The employees are well-trained professionals, a joint team who has all necessary qualities (enthusiasm, creativity, responsibility, full-bore to the job) for well co-ordinated work. You will not regret about your choice if you entrust the holding of vacancies to “East Wind”.</p>
                       </div>

                       <h3>Corporative maxims</h3>
                       <p>We bring happiness and joy our customers during their vacations in “wonderland” called Kyrgyzstan.
                           During your travel across our alpine country we create the atmosphere of comfort and warmth for you. In order to save a feeling of home comfort even if you are abroad.
                           We give a most detailed travel program. height indication, places of interest, kilometrage, time en route, etc. We try not to leave any sufficient detail which would be necessary for all our clients during travelling across Kyrgyzstan.
                           And the most important. You can have all this for acceptable price.</p>

                       <h3>Our services</h3>
                       <ul>
                           <li>We give a wide spectrum of services which are necessary for realization of any tour:</li>
                           <li>Services of guide – interpreters of English, German, French, Spanish languages.</li>
                           <li>Transport services in any part of Kyrgyz Republic.</li>
                           <li>ravelling logistics.</li>
                           <li>Tour organization of different types, duration and directions. Tracking, culture tours, horse trip, cycling tours along The Silk Way, extreme tours, mountaineering.</li>
                           <li>Rent of mountaineering equipment for a days-long walking, cycling tours and horse trips.</li>
                           <li>Visa issues and visa support</li>
                           <li>Ticket reservation</li>
                           <li>24-hour free consulting.</li>
                       </ul>

                       <h3>Our goals</h3>
                       <p>Unlike any other country Kyrgyzstan has all opportunities to develop tourism.  Ideal combination of enthusiasm and commitment  are required for gaining desirable. The main thing is to dedicate yourself to selected goal. Create, realize unprecedented ideas and overcome obstacles. Like mountaineers conquer peaks we overcome all obstacles on our way. And let an eastern tail wind help us in this interesting business.</p>
                   </div>
               </div>
            </Fragment>
        );
    }
}

export default About;