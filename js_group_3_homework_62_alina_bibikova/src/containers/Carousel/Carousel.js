import React from 'react';
import {Carousel, Col, Row} from 'react-bootstrap';
import pic1 from '../../components/Images/pic-1.jpg';
import pic2 from '../../components/Images/pic-2.jpg';
import pic3 from '../../components/Images/pic-3.jpg';

class ControlledCarousel extends React.Component {

    render() {

        return (
            <Row>
                <Col xs={12} md={12}>
                    <Carousel>
                        <Carousel.Item>
                            <img width="100%" height={500} alt="900x500" src={pic1} />
                            <Carousel.Caption>
                                <h3>First slide label</h3>
                                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img width="100%" height={500} alt="900x500" src={pic2} />
                            <Carousel.Caption>
                                <h3>Second slide label</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img width="100%" height={500} alt="900x500" src={pic3} />
                            <Carousel.Caption>
                                <h3>Third slide label</h3>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
                </Col>
            </Row>
        );
    }
}

export default ControlledCarousel;