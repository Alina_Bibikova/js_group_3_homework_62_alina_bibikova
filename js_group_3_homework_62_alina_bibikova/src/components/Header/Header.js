import React from 'react';
import './Header.css';
import logo from "../Images/logo.jpg";
import {NavLink} from 'react-router-dom';

const Header = () => {
    return (
        <header>
            <img className='logo' src={logo} alt="logo"/>
            <div className="header">
                <nav className="main-nav">
                    <ul>
                        <li><NavLink to='/home'>Home</NavLink></li>
                        <li><NavLink to='/contacts'>Contacts</NavLink></li>
                        <li><NavLink to='/about'>About Us</NavLink></li>
                    </ul>
                </nav>
            </div>
        </header>
    );
};

export default Header;